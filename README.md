# clib

This is my own implementation of various data types and utility functions in pure C, which I used for Advent of Code 2021. The implementations here are not necessarily optimal, this was just a learning exercise. In particular, to make my datastructures reusable I make heavy use of macros, which is definitely not the best way to do it (but it was fun).
