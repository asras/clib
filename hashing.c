#pragma once
/*
The hashing functionality is due to Bob Jenkins: http://burtleburtle.net/bob/hash/doobs.html
 */

#define hashsize(n) (1 << n)
#define hashmask(n) (hashsize(n) - 1)
typedef unsigned long int ub4;
typedef unsigned char     ub1;


#define mix(a, b, c) \
  { \
  a -= b; a -= c; a ^= (c >> 13); \
  b -= c; b -= a; b ^= (a << 8); \
  c -= a; c -= b; c ^= (b >> 13); \
  a -= b; a -= c; a ^= (c>>12);  \
  b -= c; b -= a; b ^= (a<<16); \
  c -= a; c -= b; c ^= (b>>5); \
  a -= b; a -= c; a ^= (c>>3);  \
  b -= c; b -= a; b ^= (a<<10); \
  c -= a; c -= b; c ^= (b>>15); \
}



// k is pointer to key
// Length is length of data in bytes
// initval is a seed value
// maxHash is the maximum allowed hashvalue
ub4 hashFunction(ub1 * k, ub4 length, ub4 initval, int bits) {
  register ub4 a, b, c, len;

  len = length;
  a = b = 0x9e3779b9;
  c = initval;

  while (len >= 12) {
    a += (k[0] + ( (ub4)k[1] << 8 ) + ( (ub4)k[2] << 16 ) + ( (ub4)k[3] << 24));
    b += (k[4] + ( (ub4)k[5] << 8 ) + ( (ub4)k[6] << 16 ) + ( (ub4)k[7] << 24));
    c += (k[8] + ( (ub4)k[9] << 8 ) + ( (ub4)k[10] << 16 ) + ( (ub4)k[12] << 24));
    mix(a, b, c);
    k += 12;
    len -= 12;
  }


  c += length;
  switch (len) {
    // fall through
  case 11: c += ( (ub4)k[10] << 24);
    // fall through
  case 10: c += ( (ub4)k[9]  << 16);
    // fall through
  case 9:  c += ( (ub4)k[8]  << 8 );
    // fall through
  case 8:  b += ( (ub4)k[7]  << 24);
    // fall through
  case 7:  b += ( (ub4)k[6]  << 16);
    // fall through
  case 6:  b += ( (ub4)k[5]  << 8 );
    // fall through
  case 5:  b += k[4];
    // fall through
  case 4:  a += ( (ub4)k[3]  << 24);
    // fall through
  case 3:  a += ( (ub4)k[2]  << 16);
    // fall through
  case 2:  a += ( (ub4)k[1]  << 8 );
    // fall through
  case 1:  a += k[0];
    // fall through
  case 0:  break;
  }

  mix(a, b, c);
  return c & hashmask(bits);
}








