/*
To use this file first #define HTABLENAME and HTABLEVAL.
HTABLENAME will be the name of the linked list type and will also
post-fix all function names, e.g. #define HTABLENAME MyHashtable -> createMyHashtable/freeMyHashtable/etc.

VAL will be the type of the data contained in the linked list, e.g. 
#define HTABLEVAL int -> MyHashtable.items will be a pointer to int.

If you redefine HTABLENAME and HTABLEVAL and re-include this file,
you can have multiple versions of a linked list, thus providing some form
of generics in C.


OBS! Hashtables also require a way to compare elements, if you want functionality
like checking for whether a value is present in the table. To do this you must define
a macro that compares two of the HTABLETYPEs. This macro must be named after the name of the table.
That is, the macro must be called <HTABLENAME>_eq.

Here is an example:

#define HTABLETYPE int
#define HTABLENAME HTable
#define int_EQ(x, y) x == y
#include "hashtable.c"
 */

#define FNAME(x, y) concat(x, y)
#define concat(x, y) x ## y


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

#define concat3(x, y, z) x ## y ## z
#define pconcat3(x, y, z) concat3(x, y, z)
#define LLISTTYPE HTABLETYPE
#define LLISTNAME pconcat3(__, HTABLENAME, HTABLELIST)
//This many levels of nested macros is needed so the preprocessor
// correctly expands them
#include "linkedlist.c"
#define LLISTTYPE HTABLETYPE
#define LLISTNAME pconcat3(__, HTABLENAME, HTABLELIST)

#include "utils.c"


#ifndef HASH_SELECTOR
#define HASH_SELECTOR(x) (unsigned char *)&x
#endif

#ifndef HASHSIZE
#define HASHSIZE(x) sizeof(x)
#endif


#define HASHSEED 0

typedef struct {
  int nbuckets;
  int bits;
  LLISTNAME **buckets;
  int elementCount;
} HTABLENAME;



// Rounds nbuckets up to nearest power of two
HTABLENAME *FNAME(create, HTABLENAME)(int nbuckets) {
  HTABLENAME *table = malloc(sizeof(HTABLENAME));

  int bits = getHighestBit(nbuckets);
  
  if ((1 << bits) != nbuckets) {
    bits++;
  }
  table->nbuckets = 1 << bits;
  table->bits = bits;
  table->buckets = malloc(table->nbuckets * sizeof(LLISTNAME *));
  for (int i = 0; i < table->nbuckets; i++) {
    table->buckets[i] = NULL;
  }

  table->elementCount = 0;

  return table;
}


HTABLENAME *FNAME(createMany, HTABLENAME)(int tableCount, int nbuckets) {
  HTABLENAME *table = malloc(sizeof(HTABLENAME) * tableCount);

  int bits = getHighestBit(nbuckets);
  
  if ((1 << bits) != nbuckets) {
    bits++;
  }

  for (int i = 0; i < tableCount; i++) {
    table[i].nbuckets = 1 << bits;
    table[i].bits = bits;
    table[i].buckets = malloc(table[i].nbuckets * sizeof(LLISTNAME *));
    for (int j = 0; j < table[i].nbuckets; j++) {
      table[i].buckets[j] = NULL;
    }
    table[i].elementCount = 0;
  }

  return table;
}


void FNAME(free, HTABLENAME)(HTABLENAME * table) {
  assert(table != NULL);
  for (int i = 0; i < table->nbuckets; i++) {
    if (table->buckets[i] != NULL) {
      FNAME(free, LLISTNAME)(table->buckets[i]);
    }
  }
  free(table->buckets);
  free(table);
}

void FNAME(freeSingle, HTABLENAME)(HTABLENAME * table) {
  assert(table != NULL);
  for (int i = 0; i < table->nbuckets; i++) {
    if (table->buckets[i] != NULL) {
      FNAME(free, LLISTNAME)(table->buckets[i]);
    }
  }
  free(table->buckets);
}


void FNAME(freeMany, HTABLENAME)(HTABLENAME * table, int tableCount) {
  assert(table != NULL);
  for (int i = 0; i < tableCount; i++) {
    FNAME(freeSingle, HTABLENAME)(&table[i]);
  }
  free(table);
}


void FNAME(deleteAllElems, HTABLENAME)(HTABLENAME * table) {
  for (int i = 0; i < table->nbuckets; i++) {
    if (table->buckets[i] != NULL) {
      FNAME(free, LLISTNAME)(table->buckets[i]);
      table->buckets[i] = NULL;
    }
  }
  table->elementCount = 0;
}



#include "hashing.c"


bool FNAME(bucketElem, HTABLENAME)(LLISTNAME * bucket, HTABLETYPE value) {
  LLISTNAME * current = bucket;
  while (current != NULL) {
    HTABLETYPE curVal = current->value;
    if (FNAME(HTABLETYPE, _EQ)(value, curVal)) {
      return true;
    }
    current = current -> next;
  }
  return false;
}


// ELEM: Check if a value is in the hashtable
bool FNAME(elem, HTABLENAME)(HTABLENAME * table, HTABLETYPE value) {
  int bucketNumber = hashFunction(HASH_SELECTOR(value), HASHSIZE(value), HASHSEED, table->bits);

  return FNAME(bucketElem, HTABLENAME)(table->buckets[bucketNumber], value);
}


// INSERT: Insert a new value into the hashtable
void FNAME(insert, HTABLENAME)(HTABLENAME * table, HTABLETYPE value) {
  if (FNAME(elem, HTABLENAME)(table, value)) {
    return;
  }

  int bucketNumber = hashFunction(HASH_SELECTOR(value), HASHSIZE(value), HASHSEED, table->bits);
  if (table->buckets[bucketNumber] == NULL) {
    table->buckets[bucketNumber] = FNAME(create, LLISTNAME)(value);
    table->elementCount++;
  } else {
    table->buckets[bucketNumber] = FNAME(cons, LLISTNAME)(table->buckets[bucketNumber], value);
    table->elementCount++;
  }
}




LLISTNAME * FNAME(bucketDelete, HTABLENAME)(LLISTNAME * bucket, HTABLETYPE value) {
  if (FNAME(HTABLETYPE, _EQ)(bucket->value, value)) {
    LLISTNAME * newHead = bucket->next;
    FNAME(freeNode, LLISTNAME)(bucket);
    return newHead;
  } else {
    LLISTNAME * prev = bucket;
    LLISTNAME * curr = bucket->next;
    bool found = false;

    while (curr != NULL) {
      if (FNAME(HTABLETYPE, _EQ)(curr->value, value)) {
        found = true;
        prev->next = curr->next;
        FNAME(freeNode, LLISTNAME)(curr);
        break;
      }
      prev = curr;
      curr = curr->next;
    }

    assert(found);

    // Head remains the same
    return bucket;
  }
}


void FNAME(deleteElem, HTABLENAME)(HTABLENAME * table, HTABLETYPE value) {
  if (!FNAME(elem, HTABLENAME)(table, value)) {
    return;
  }
  int bucketNumber = hashFunction(HASH_SELECTOR(value), HASHSIZE(value), HASHSEED, table->bits);

  LLISTNAME *newHead = FNAME(bucketDelete, HTABLENAME)(table->buckets[bucketNumber], value);
  table->elementCount--;
  table->buckets[bucketNumber] = newHead;
  return;
}


#define LOOPTABLE(table, nodename, body) \
for (int __IDX = 0; __IDX < table->nbuckets; __IDX++) { \
    if (table->buckets[__IDX] == NULL) { \
      continue; \
    } else { \
      for (LLISTNAME * nodename = table->buckets[__IDX]; nodename != NULL; nodename = nodename ->next) { \
        body \
      } \
    } \
 } \



void FNAME(union, HTABLENAME)(HTABLENAME * table1, HTABLENAME * table2) {
  LOOPTABLE(table2, curr,
            FNAME(insert, HTABLENAME)(table1, curr->value););
  return;
}

void FNAME(difference, HTABLENAME)(HTABLENAME * table1, HTABLENAME * table2) {
  LOOPTABLE(table2, curr,
            FNAME(deleteElem, HTABLENAME)(table1, curr->value););
  return;
}

HTABLENAME * FNAME(intersectionCreate, HTABLENAME)(HTABLENAME * table1, HTABLENAME * table2) {
  HTABLENAME * result = FNAME(create, HTABLENAME)(table1->nbuckets);
  LOOPTABLE(table1, curr,
            if (FNAME(elem, HTABLENAME)(table2, curr->value)) {
              FNAME(insert, HTABLENAME)(result, curr->value);
            });
  return result; 
}

void FNAME(intersection, HTABLENAME)(HTABLENAME * table1, HTABLENAME * table2) {
  HTABLENAME * temp = FNAME(intersectionCreate, HTABLENAME(table1, table2));
  FNAME(deleteAllElems, HTABLENAME)(table1);

  FNAME(union, HTABLENAME)(table1, temp);

  FNAME(free, HTABLENAME)(temp);
}



#ifdef PRINT_INT_HTABLE
void FNAME(printBucket, HTABLENAME)(LLISTNAME * bucket) {
  LLISTNAME * current = bucket;
  if (current == NULL) {
    puts("    Nothing");
    return;
  }
  while (current != NULL) {
    printf("    %i\n", current->value);
    current = current->next;
  }
}

void FNAME(print, HTABLENAME)(HTABLENAME * table) {
  puts("Table contains:");
  for (int i = 0; i < table->nbuckets; i++) {
    printf("  Bucket %i contains:\n", i);
    FNAME(printBucket, HTABLENAME)(table->buckets[i]);
  }
}
#endif

#ifdef PRINT_DOUBLE_HTABLE
void FNAME(printBucket, HTABLENAME)(LLISTNAME * bucket) {
  LLISTNAME * current = bucket;
  if (current == NULL) {
    puts("    Nothing");
    return;
  }
  while (current != NULL) {
    printf("    %f\n", current->value);
    current = current->next;
  }
}

void FNAME(print, HTABLENAME)(HTABLENAME * table) {
  puts("Table contains:");
  for (int i = 0; i < table->nbuckets; i++) {
    printf("  Bucket %i contains:\n", i);
    FNAME(printBucket, HTABLENAME)(table->buckets[i]);
  }
}
#endif


#ifdef PRINT_CHAR_TABLE
void FNAME(printBucket, HTABLENAME)(LLISTNAME * bucket) {
  LLISTNAME * current = bucket;
  if (current == NULL) {
    puts("    Nothing");
    return;
  }
  while (current != NULL) {
    printf("    %c\n", current->value);
    current = current->next;
  }
}

void FNAME(print, HTABLENAME)(HTABLENAME * table) {
  puts("Table contains:");
  for (int i = 0; i < table->nbuckets; i++) {
    printf("  Bucket %i contains:\n", i);
    FNAME(printBucket, HTABLENAME)(table->buckets[i]);
  }
}
#endif
