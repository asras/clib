#include <stdbool.h>

#define FNAME(x, y) concat(x, y)
#define concat(x, y) x ## y


void FNAME(heapsort, SORTTYPE)(size_t n, SORTTYPE * arr) {
  size_t l = (n >> 1) + 1;
  
  size_t ir = n;

  size_t i, j;

  SORTTYPE rra;
  while (true) {
    if (l > 1) {
      rra = arr[--l - 1];
    } else {
      rra = arr[ir - 1];
      arr[ir - 1] = arr[0];
      if (--ir == 0) {
        arr[0] = rra;
        return;
      }      
    }
    i = l;
    j = l << 1;
    while (j <= ir) {
      if (j < ir && FNAME(SORTTYPE, _GEQ)(arr[j], arr[j - 1])) {
        ++j;
      }
      if (FNAME(SORTTYPE, _GEQ)(arr[j - 1], rra)) {
        arr[i - 1] = arr[j - 1];
        j += (i = j);
      } else {
        j = ir + 1;
      }
    }
    
    arr[i - 1] = rra;
  }
}


void FNAME(heapsort2, SORTTYPE)(size_t n, SORTTYPE * arr, SORTTYPE * arr2) {
  size_t l = (n >> 1) + 1;
  
  size_t ir = n;

  size_t i, j;

  SORTTYPE rra, rrb;
  while (true) {
    if (l > 1) {
      rra = arr[--l - 1];
      rrb = arr2[l - 1];
    } else {
      rra = arr[ir - 1];
      rrb = arr2[ir - 1];
      arr[ir - 1] = arr[0];
      arr2[ir - 1] = arr2[0];
      if (--ir == 0) {
        arr[0] = rra;
        arr2[0] = rrb;
        return;
      }      
    }
    i = l;
    j = l << 1;
    while (j <= ir) {
      if (j < ir && FNAME(SORTTYPE, _GEQ)(arr[j], arr[j - 1])) {
        ++j;
      }
      if (FNAME(SORTTYPE, _GEQ)(arr[j - 1], rra)) {
        arr[i - 1] = arr[j - 1];
        arr2[i - 1] = arr2[j - 1];
        j += (i = j);
      } else {
        j = ir + 1;
      }
    }
    
    arr[i - 1] = rra;
    arr2[i - 1] = rrb;
  }
}
