
#define FNAME(x, y) concat(x, y)
#define concat(x, y) x ## y
#define FRONT_MAX 100

#include <stddef.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef P_CMP
#define P_CMP(x, y) x >= y
#endif

typedef struct {
  QUEUETYPE value;
  int priority;
} FNAME(PVALUE, QUEUENAME);

#define getpqvalue(x) x->value.value
#define getpqp(x) x->value.priority

#define concat3(x, y, z) x ## y ## z
#define pconcat3(x, y, z) concat3(x, y, z)
#define LLISTTYPE FNAME(PVALUE, QUEUENAME)
#define LLISTNAME pconcat3(__, QUEUENAME, QUEUELIST)
//This many levels of nested macros is needed so the preprocessor
// correctly expands them
#include "linkedlist.c"


typedef struct {
  LLISTNAME * items;
} QUEUENAME;


QUEUENAME * FNAME(create, QUEUENAME)() {
  QUEUENAME * queue = malloc(sizeof(QUEUENAME));
  
  LLISTNAME * items = NULL;
  queue->items = items;
  
  return queue;
}

void FNAME(free, QUEUENAME)(QUEUENAME * queue) {
  FNAME(free, LLISTNAME)(queue->items);
  free(queue);
}

/* static void FNAME(resize, QUEUENAME)(QUEUENAME * queue) { */
/*   queue->capacity *= 2; */
/*   queue->items = realloc(queue->items, queue->capacity * sizeof(QUEUETYPE)); */
/*   queue->priorities = realloc(queue->priorities, queue->capacity * sizeof(int)); */
/*   assert(queue->items != NULL); */
/*   assert(queue->priorities != NULL); */
/* } */


void FNAME(enqueue, QUEUENAME)(QUEUENAME * queue, QUEUETYPE value, int priority) {
  LLISTNAME *current = queue->items;
  
  FNAME(PVALUE, QUEUENAME) entry;
  entry.value = value;
  entry.priority = priority;
  
  if (current == NULL) {
    queue->items = FNAME(create, LLISTNAME)(entry);
    return;
  } else {
    LLISTNAME *prev = NULL;
    while (current != NULL) {
      if (P_CMP(priority, getpqp(current))) {
        LLISTNAME *new = FNAME(create, LLISTNAME)(entry);
        new->next = current;
        if (prev != NULL) {
          prev->next = new;
        } else {
          queue->items = new;
        }
        return;
      }

      prev = current;
      current = current->next;
    }
    LLISTNAME *new = FNAME(create, LLISTNAME)(entry);
    prev->next = new;
  }
}


QUEUETYPE FNAME(dequeue, QUEUENAME)(QUEUENAME * queue) {
  if (queue->items == NULL) {
    assert(0 && "Queue is empty!");
  }
  LLISTNAME *next = queue->items->next;

  QUEUETYPE val = getpqvalue(queue->items);

  queue->items = next;

  return val;
}

bool FNAME(empty, QUEUENAME)(QUEUENAME * queue) {
  return queue->items == NULL;
}
