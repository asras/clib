/*
To use this file first #define QUEUENAME and QUEUEVAL.
QUEUENAME will be the name of the linked list type and will also
post-fix all function names, e.g. #define QUEUENAME MyQueue -> createMyQueue/freeMyQueue/etc.

VAL will be the type of the data contained in the linked list, e.g. 
#define QUEUEVAL int -> MyQueue.items will be a pointer to int.

If you redefine QUEUENAME and QUEUEVAL and re-include this file,
you can have multiple versions of a linked list, thus providing some form
of generics in C.
 */

#define FNAME(x, y) concat(x, y)
#define concat(x, y) x ## y
#define FRONT_MAX 100

#include <stddef.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct {
  QUEUETYPE * items;
  int capacity;
  int front;
  int back;
} QUEUENAME;


QUEUENAME * FNAME(create, QUEUENAME)(int capacity) {
  QUEUENAME * queue = malloc(sizeof(QUEUENAME));
  
  QUEUETYPE * items = malloc(capacity * sizeof(QUEUETYPE));
  queue->items = items;

  queue->capacity = capacity;
  queue->front = -1;
  queue->back = -1;
  
  return queue;
}

void FNAME(free, QUEUENAME)(QUEUENAME * queue) {
  free(queue->items);
  free(queue);
}

static void FNAME(resize, QUEUENAME)(QUEUENAME * queue) {
  queue->capacity *= 2;
  queue->items = realloc(queue->items, queue->capacity * sizeof(QUEUETYPE));
  assert(queue->items != NULL);
}


void FNAME(enqueue, QUEUENAME)(QUEUENAME * queue, QUEUETYPE value) {
  if (queue->back == queue->capacity - 1) {
    FNAME(resize, QUEUENAME)(queue);
  }
  queue->back++;
  if (queue->front == -1) {
    queue->front++;
  }

  queue->items[queue->back] = value;
}


QUEUETYPE FNAME(dequeue, QUEUENAME)(QUEUENAME * queue) {
  if (queue->front > queue->back) {
    assert(0 && "Queue is empty!");
  }
  if (queue->front >= FRONT_MAX) {
    assert(queue->front == FRONT_MAX);
    int queueSize = queue->back - queue->front + 1;
    for (int i = 0; i < queueSize; i++) {
      QUEUETYPE * dst = &(queue->items[i]);
      QUEUETYPE * src = &(queue->items[i + queue->front]);
      memcpy(dst, src, sizeof(QUEUETYPE));
    }
    queue->front = 1;
    queue->back = queueSize - 1;
    return queue->items[0];
  } else {
    queue->front++;
    return queue->items[queue->front - 1];
  }
}

bool FNAME(empty, QUEUENAME)(QUEUENAME * queue) {
  return queue->front > queue->back;
}
