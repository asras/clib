#include <stdio.h>
#include <stdlib.h>

typedef struct {
  char **lines;
  int *lineLengths;
  int numberOfLines;
} FileContent;

void freeFileContent(FileContent *fc) {
  for (int i = 0; i < fc->numberOfLines; i++) {
    free(fc->lines[i]);
  }

  free(fc->lines);
  free(fc);
}


void freeFileString(char *s) {
  free(s);
}

char *readFileString(char *fname) {
  FILE *file = fopen(fname, "r");
  if (file == NULL) {
    return NULL;
  }
  char ch;
  int charCount = 0;
  while ((ch = fgetc(file)) != EOF) {
    charCount++;
  }
  rewind(file);

  char *fs = malloc(sizeof(char) * (charCount + 1));
  int i = 0;
  while ((ch = fgetc(file)) != EOF) {
    fs[i] = ch;
    i++;
  }
  fs[charCount] = '\0';    

  return fs;
}
  

FileContent* readFile(char *fname) {
  FILE *file = fopen(fname, "r");

  if (file == NULL) {
    return NULL;
  }

  char ch;
  
  int lineCount = 0;
  int currCount = 0;
  while ((ch = fgetc(file)) != EOF) {
    currCount++;
    if (ch == '\n') {
      lineCount++;
      currCount = 0;
    }
  }
  if (currCount != 0) {
    lineCount++; // Count the last line
  }
  rewind(file);

  int *lineLengths= (int*) malloc(lineCount * sizeof(int));
  
  int lineNum = 0;
  int currentLength = 0;
  while ((ch = fgetc(file)) != EOF) {
    currentLength++;
    if (ch == '\n') {
      lineLengths[lineNum] = currentLength;
      lineNum++;
      currentLength = 0;
    }
  }
  if (currentLength != 0) {
    lineLengths[lineNum + 1] = currentLength + 1;
  }
  rewind(file);

  char **lineContent = (char**) malloc(lineCount * sizeof(char*));
  for (int i = 0; i < lineCount; i++) {
    lineContent[i] = (char*) malloc(lineLengths[i] * sizeof(char));
  }
  
  lineNum = 0;
  currentLength = 0;
  while ((ch = fgetc(file)) != EOF) {
    if (ch == '\n') {
      lineContent[lineNum][currentLength] = '\0';
      lineNum++;
      currentLength = 0;

      //      printf("Current linenum: %i\n", lineNum);
      continue;
    }
    lineContent[lineNum][currentLength] = ch;
    currentLength++;
    
  }  
  fclose(file);


  FileContent *fc = malloc(sizeof(FileContent));
  fc->lines = lineContent;
  fc->lineLengths = lineLengths;
  fc->numberOfLines = lineCount;
  
  return fc; 
}
