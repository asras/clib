/*
To use this file first #define STACKNAME and STACKVAL.
STACKNAME will be the name of the linked list type and will also
post-fix all function names, e.g. #define STACKNAME MyStack -> createMyStack/freeMyStack/etc.

VAL will be the type of the data contained in the linked list, e.g. 
#define STACKVAL int -> MyStack.items will be a pointer to int.

If you redefine STACKNAME and STACKVAL and re-include this file,
you can have multiple versions of a linked list, thus providing some form
of generics in C.
 */

#define FNAME(x, y) concat(x, y)
#define concat(x, y) x ## y

#include <stddef.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct STACKNAME {
  STACKTYPE * items;
  int capacity;
  int current;
} STACKNAME;


STACKNAME * FNAME(create, STACKNAME)(int capacity) {
  STACKNAME* stack = malloc(sizeof(STACKNAME));
  
  STACKTYPE * items = malloc(capacity * sizeof(STACKTYPE));

  stack->items = items;
  stack->capacity = capacity;
  stack->current = -1;

  return stack;
}


void FNAME(free, STACKNAME)(STACKNAME* stack) {
  free(stack->items);
  free(stack);
}


static void FNAME(resize, STACKNAME)(STACKNAME* stack) {
  stack->capacity *= 2;
  stack->items = realloc(stack->items, stack->capacity * sizeof(STACKTYPE));
  assert(stack->items != NULL);
}


void FNAME(push, STACKNAME)(STACKNAME* stack, STACKTYPE item) {
  if (stack->current == stack->capacity - 1) {
    FNAME(resize, STACKNAME)(stack);
  }
  stack->current++;
  stack->items[stack->current] = item;
}

  
STACKTYPE FNAME(pop, STACKNAME)(STACKNAME* stack) {
  if (stack->current == -1) {
    assert(0 && "Stack is empty");
  }

  stack->current--;
  return stack->items[stack->current + 1];
}

bool FNAME(empty, STACKNAME)(STACKNAME *stack) {
  return stack->current < 0;
}
