#include <stdbool.h>

typedef struct {
    char *ptr;
    int len;
} StringView;

char* sv_toString(StringView sv) {
    char *string = malloc(sizeof(char) * (sv.len + 1));

    for (int i = 0; i < sv.len; i++) {
        string[i] = sv.ptr[i];
    }
    string[sv.len] = '\0';

    return string;
}


int sv_atoi(StringView sv) {
  char *s = sv_toString(sv);
  int val = atoi(s);
  free(s);
  return val;
}


StringView takeWord(char *string) {
    StringView sv = {0};

    int idx = 0;
    char c = string[idx];
    int len = 0;
    while (c != '\0' && c != '\n' && c != ' ') {
        len++;
        c = string[len];
    }
    sv.ptr = string;
    sv.len = len;

    return sv;
}

bool sv_eq(StringView sv1, StringView sv2) {
    if (sv1.len != sv2.len) {
        return false;
    }

    for (int i = 0; i < sv1.len; i++) {
        if (sv1.ptr[i] != sv2.ptr[i]) {
            return false;
        }
    }
    return true;
}

bool svs_eq(StringView sv, const char * s) {
    for (int i = 0; i < sv.len; i++) {
        if (s[i] == '\0') {
            return false;
        }
        if  (s[i] !=  sv.ptr[i]) {
            return false;
        }
    }
    return true;
}


StringView sv_fromString(char * s) {
  StringView sv;

  sv.ptr = s;
  int len = 0;
  while (s[len] != '\0') len++;
  sv.len = len;

  return sv;
}


bool lineend(char c) {
  return c == '\n' || c == EOF;
}

bool wordend(char c) {
  return c == ' ' || lineend(c);
}

bool empty(char c) {
  return c == ' ' || c == '\t';
}


StringView sv_chopX(StringView *sv, bool (*checker)(char)) {
  StringView x;

  x.ptr = sv->ptr;
  int len = 0;
  while (!(*checker)(sv->ptr[len]) && (sv->ptr[len] != EOF) && (sv->len - len) > 0) len++;

  x.len = len;
  sv->ptr = sv->ptr + len;
  sv->len = sv->len - len;

  return x;
}

StringView sv_softchopX(StringView sv, bool (*checker)(char)) {
  StringView x;

  x.ptr = sv.ptr;
  int len = 0;
  while (!(*checker)(sv.ptr[len]) && sv.ptr[len] != EOF) len++;

  x.len = len;

  return x;
}

void sv_dropX(StringView *sv, bool (*checker)(char)) {
  int len = 0;
  while (!(*checker)(sv->ptr[len]) && sv->ptr[len] != EOF) len++;

  sv->ptr = sv->ptr + len;
  sv->len = sv->len - len;
}


StringView sv_chopWord(StringView *sv) {
  return sv_chopX(sv, &wordend);
}

void sv_dropWord(StringView *sv) {
  sv_dropX(sv, &wordend);
}


StringView sv_chopLine(StringView *sv) {
  StringView result = sv_chopX(sv, &lineend);

  // Remove newline char
  if (sv->len > 0) {
    sv->ptr++;
    sv->len--;
  }

  return result; 
}

StringView sv_softchopLine(StringView sv) {
  return sv_softchopX(sv, &lineend);
}


void sv_dropLine(StringView *sv) {
  sv_dropX(sv, &lineend);
}

/* void sv_dropChar(StringView *sv, char c) { */
/*   int len = 0; */
/*   while (sv->ptr[len] == c) len++; */

/*   sv->ptr = sv->ptr + len; */
/*   sv->len = sv->len - len; */
/* } */


void sv_trim(StringView *sv) {
  int i = 0;

  while (empty(sv->ptr[i]) && (sv->ptr[i] != EOF)) i++;

  sv->ptr += i;
  sv->len -= i;
}


void sv_print(StringView sv) {
  for (int i = 0; i < sv.len; i++) {
    printf("%c", sv.ptr[i]);
  }
  puts("");
}

void sv_write(StringView sv) {
  for (int i = 0; i < sv.len; i++) {
    printf("%c", sv.ptr[i]);
  }
}


StringView sv_chopOnChar(StringView *sv, char c) {
  StringView chopped;

  chopped.ptr = sv->ptr;
  int len = 0;
  while ((sv->ptr[len] != c) && (len < sv->len)) {
    len++;
  }

  chopped.len = len;
  sv->ptr = sv->ptr + len;
  sv->len = sv->len - len;

  return chopped;
}



StringView sv_chopOnChars(StringView *sv, char *cs) {
  StringView chopped;

  chopped.ptr = sv->ptr;
  int len = 0;
  while ((len < sv->len)) {
    bool found = false;
    int loc = 0;
    char c = cs[loc];
    while (c != '\0') {
      if (sv->ptr[len] == c) {
        found = true;
        break;
      }
      loc++;
      c = cs[loc];
    }
    if (found) {
      break;
    }
    len++;
  }

  chopped.len = len;
  sv->ptr = sv->ptr + len;
  sv->len = sv->len - len;

  return chopped;
}

void sv_dropChar(StringView *sv, char c) {
  int len = 0;
  while ((sv->ptr[len] == c) && (len < sv->len)) {
    len++;
  }

  sv->ptr = sv->ptr + len;
  sv->len = sv->len - len;
}


int sv_countLines(StringView sv) {
  int c = 1;
  for (int i = 0; i < sv.len; i++) {
    if (sv.ptr[i] == '\n') {
      c++;
    }
  }

  return c;
}

void sv_strip(StringView *sv) {
  int len = 0;
  while ((lineend(sv->ptr[len]) || empty(sv->ptr[len])) && (len < sv->len)) len++;

  sv->ptr = sv->ptr + len;
  sv->len = sv->len - len;

  len = sv->len - 1;
  while ((lineend(sv->ptr[len]) || empty(sv->ptr[len])) && (len >= 0)) len--;

  sv->len = len + 1;
}

int sv_countChar(StringView sv, char c) {
  int count = 0;
  for (int i = 0; i < sv.len; i++) {
    if (sv.ptr[i] == c) {
      count++;
    }
  }

  return count;
}


int sv_findChar(StringView sv, char c) {
  int position = -1;
  for (int i = 0; i < sv.len; i++) {
    if (sv.ptr[i] == c) {
      position = i;
      break;
    }
  }

  return position;
}


bool sv_allLowerCase(StringView sv) {
  for (int i = 0; i < sv.len; i++) {
    char c = sv.ptr[i];
    if (c < 97 || c > 122) {
      return false;
    }
  }

  return true;
}


void sv_dropN(StringView *sv, int n) {
  int m;
  if (sv->len > n) {
    m = n;
  } else {
    m = sv->len;
  }
  sv->ptr = sv->ptr + m;
  sv->len = sv->len - m;
}

// TODO Functionality to "get all lines" from a string(view)
// TODO Functionality to "get all non-empty lines" from a string(view)
// TODO Functionality to "split on" which returns a list of StringViews:
//      splitOn(0,9 -> 5,9, ' ') -> ["0,9", "->", "5,9"]
