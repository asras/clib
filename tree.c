/*
To use this file first #define TREENAME and TREEVAL.
TREENAME will be the name of the linked list type and will also
post-fix all function names, e.g. #define TREENAME MyTree -> createMyTree/freeMyTree/etc.

VAL will be the type of the data contained in the linked list, e.g. 
#define TREEVAL int -> MyTree.nodeValue will be an int.

If you redefine TREENAME and TREEVAL and re-include this file,
you can have multiple versions of a linked list, thus providing some form
of generics in C.

Trees also have a predefined print function. To include these do
#define INCLUDE_PRINT_INT_TREE

(and similar for other types)

before you include this file. See the bottom of this file for what types have a print function.

 */

#define FNAME(x, y) concat(x, y)
#define concat(x, y) x ## y

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

typedef struct TREENAME {
  int branchNumber;
  int branchCapacity;
  TREEVAL nodeValue;
  struct TREENAME **branches;
} TREENAME;


TREENAME * FNAME(create, TREENAME)(TREEVAL nodeValue, int branchCapacity) {
  TREENAME * tree = malloc(sizeof(TREENAME));

  tree->branchNumber = 0;
  
  tree->branchCapacity = branchCapacity;

  tree->nodeValue = nodeValue;

  TREENAME ** branches = malloc(branchCapacity * sizeof(TREENAME *));
  tree->branches = branches;

  return tree;
}


void FNAME(free, TREENAME)(TREENAME * tree) {
  for (int i = 0; i < tree->branchNumber; i++) {
    if (tree->branches[i] != NULL) {
      FNAME(free, TREENAME)(tree->branches[i]);
    }
  }
  free(tree);
}


static void FNAME(resize, TREENAME)(TREENAME * tree) {
  tree->branchCapacity *= 2;
  tree->branches = realloc(tree->branches, tree->branchCapacity * sizeof(TREENAME *));
  assert(tree->branches != NULL);
}


void FNAME(insertSub, TREENAME)(TREENAME * tree, TREENAME * subtree) {
  if (tree->branchNumber == tree->branchCapacity) {
    FNAME(resize, TREENAME)(tree);
  }

  tree->branches[tree->branchNumber] = subtree;
  tree->branchNumber++;
}

#ifdef INCLUDE_PRINT_INT_TREE
void printIntTree(TREENAME * tree, const char * prefix) {
  printf("%s Value: %i\n", prefix, tree->nodeValue);
  char *newPre = malloc(strlen(prefix) + strlen("  ") + 1);
  strcpy(newPre, "  ");
  strcat(newPre, prefix);
  for (int i = 0; i < tree->branchNumber; i++) {
    printIntTree(tree->branches[i], newPre);
  }
  free(newPre);
}
#endif

